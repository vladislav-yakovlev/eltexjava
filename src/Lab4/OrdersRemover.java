package Lab4;

public class OrdersRemover extends ACheck {
    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(waitMillis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            synchronized (orders) {
                orders.removeProcessedOrders();
            }
        }
    }
}
