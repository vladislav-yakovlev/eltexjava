package Lab4;

public class OrdersChecker extends ACheck {
    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(waitMillis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            synchronized (orders) {
                orders.checkOrders();
            }
        }
    }
}
