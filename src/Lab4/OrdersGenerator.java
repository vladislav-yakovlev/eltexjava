package Lab4;

import Lab2and3.Order;
import Lab2and3.Orders;
import Lab2and3.OrdersFactory;

public class OrdersGenerator extends Thread {
    Orders<Order> orders;
    long waitMillis;

    OrdersGenerator(Orders orders, long waitMillis) {
        this.orders = orders;
        this.waitMillis = waitMillis;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(waitMillis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            synchronized (orders) {
                orders.makePurchase(OrdersFactory.getRandomOrder());
            }
        }
    }
}
