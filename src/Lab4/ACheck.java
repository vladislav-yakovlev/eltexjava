package Lab4;

import Lab2and3.Orders;

public abstract class ACheck implements Runnable {
    Orders orders;
    Boolean status;
    long waitMillis;

    public ACheck() {
    }

    /**
     * @param orders     list of orders.
     * @param status
     * @param waitMillis
     */
    public ACheck(Orders orders, boolean status, long waitMillis) {
        this.orders = orders;
        this.status = status;
        this.waitMillis = waitMillis;
    }
}
