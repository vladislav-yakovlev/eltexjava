package Lab6;

import java.io.*;
import java.net.*;

public class TestServer implements Runnable {
    int clientPort;
    //String data;
    //byte[] dataBytes;

    public TestServer(int clientPort) {
        this.clientPort = clientPort;
        //data = "8555";
        //dataBytes = new byte[128];
    }

    @Override
    public void run() {
        try (DatagramSocket serverSocket = new DatagramSocket(50000)) {
            for (int i = 0; i < 3; i++) {
                String message = "Message number " + i;
                DatagramPacket datagramPacket = new DatagramPacket(message.getBytes(), message.length(), InetAddress.getLocalHost(), clientPort);
                serverSocket.send(datagramPacket);
                //System.exit(0);
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
