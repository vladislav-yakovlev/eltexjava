package Lab6;

import Lab2and3.Order;
import Lab2and3.Orders;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerThread <T extends Order> implements Runnable {
    //int serverPort;
    ServerSocket serverSocket;
    int clientPort;
    Orders orders;

    public ServerThread(ServerSocket serverSocket, int clientPort, Orders<Order> orders) {
        this.serverSocket = serverSocket;
        this.clientPort = clientPort;
        this.orders = orders;
    }

    @Override
    public void run() {
        Socket socket;
        //(ServerSocket server = new ServerSocket(serverPort);
             //DatagramSocket data = new DatagramSocket(serverPort);
             //BufferedReader reader = new BufferedReader(new InputStreamReader(server.get)))
        try{
            while(true) {
                socket = serverSocket.accept();
                System.out.println(socket.getInetAddress().getHostName() + " connected");
                System.out.println(socket.getInetAddress());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
