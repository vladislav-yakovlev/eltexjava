package Lab6;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class TestClient implements Runnable {
    int port;

    public TestClient(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        try (DatagramSocket clientSocket = new DatagramSocket(port)) {
            byte[] buffer = new byte[256];
            clientSocket.setSoTimeout(3000);

            for (int i = 0; i < 3; i++) {
                DatagramPacket datagramPacket = new DatagramPacket(buffer, 0, buffer.length);
                clientSocket.receive(datagramPacket);

                String receivedMessage = new String(datagramPacket.getData());
                System.out.println(receivedMessage);
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
