package Lab6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.net.*;

public class Client implements Runnable {
    int clientPort;
    int serverPort;

    public Client(int clientPort, int serverPort) {
        this.clientPort = clientPort;
        this.serverPort = serverPort;
    }

    @Override
    public void run() {
        try (DatagramSocket clientSocket = new DatagramSocket(clientPort)) {
            byte[] buffer = new byte[256];
            //clientSocket.setSoTimeout(3000);
            //for (int i = 0; i < 3; i++) {
            DatagramPacket datagramPacket = new DatagramPacket(buffer, 0, buffer.length);
            clientSocket.receive(datagramPacket);
            String receivedMessage = new String(datagramPacket.getData());
            System.out.println(receivedMessage);

            Socket socket = new Socket(InetAddress.getLocalHost(), serverPort);
            PrintStream printStream = new PrintStream(socket.getOutputStream());
            printStream.println("aaa");
            /*for (int i = 0; i < 5; i++) {
                System.out.print(i);
            }
            System.out.println();
            System.out.println("Client " + Thread.currentThread().getName() + " finished");*/
            //}
            //clientSocket.close();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
