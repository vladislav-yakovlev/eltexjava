package Lab6;

import Lab2and3.Order;
import Lab2and3.Orders;

import java.io.IOException;
import java.net.*;
import java.util.Collection;
import java.util.List;

public class Server<T extends Order> implements Runnable {
    Orders<T> orders;
    int clientsNumber;
    int serverPort;

    public Server(int clientsNumber, int serverPort, Orders<T> orders) {
        this.clientsNumber = clientsNumber;
        this.serverPort = serverPort;
        this.orders = orders;
    }

    @Override
    public void run() {
        try (DatagramSocket serverSocket = new DatagramSocket(serverPort)) {
            for (int i = 1; i <= clientsNumber; i++) {
                //String message = "Message for port " + i;
                //String message = "Message for port : " + (serverPort + i) + ", port for orders: " + String.valueOf(port);
                int serverThreadPort = serverPort + i;
                int clientPort = serverPort + clientsNumber + i;
                String message = String.valueOf(serverThreadPort);
                DatagramPacket datagramPacket = new DatagramPacket(message.getBytes(), message.length(), InetAddress.getLocalHost(), clientPort);
                serverSocket.send(datagramPacket);

                ServerSocket serverThreadSocket = new ServerSocket(serverPort + i);
                new Thread(new ServerThread(serverThreadSocket, clientPort, orders)).start();
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
