package Lab5;

import Lab2and3.Order;

import java.io.*;
import java.util.ArrayList;
import java.util.UUID;

public class ManagerOrderFile extends AManageOrder implements IOrder {
    private final String defaultPath = "byteFile.dat";

    public ManagerOrderFile() {
        this.path = defaultPath;
    }

    public ManagerOrderFile(String path) {
        super(path);
    }

    @Override
    public Order readById(UUID id) {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(path))) {
            Order order;
            while ((order = (Order) objectInputStream.readObject()) != null) {
                if (order.getId().compareTo(id) == 0) {
                    orders.add(order);
                    return order;
                }
            }
        } catch (ClassNotFoundException e) {
            System.err.println("Class does not exist");
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
            e.printStackTrace();
        } catch (InvalidClassException e) {
            System.err.println("Class version mismatch");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Input error");
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public ArrayList<Order> readAll() {
        ArrayList<Order> result = new ArrayList<>();

        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(path))) {
            Order order;
            while ((order = (Order) objectInputStream.readObject()) != null) {
                orders.add(order);
                result.add(order);
            }
        } catch (ClassNotFoundException e) {
            System.err.println("Class does not exist");
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
            e.printStackTrace();
        } catch (InvalidClassException e) {
            System.err.println("Class version mismatch");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Input error");
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean saveById(UUID id) {
        for (Order order : orders) {
            //Order order = (Order) orders.get(i);
            if (order.getId().compareTo(id) == 0) {
                //boolean flag = false;

                try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path, true))) {
                    objectOutputStream.writeObject(order);
                    objectOutputStream.flush();
                    //flag = true;
                    return true;
                } catch (FileNotFoundException e) {
                    System.err.println("The file could not be created");
                    e.printStackTrace();
                } catch (NotSerializableException e) {
                    System.err.println("The class does not support serialization");
                    e.printStackTrace();
                } catch (IOException e) {
                    System.err.println("Output error");
                    e.printStackTrace();
                }

                //return flag;
            }
        }

        return false;
    }

    @Override
    public int saveAll() {
        //Order order = (Order) orders.get(i);
        int count = 0;
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path, true))) {
            for (Order order : orders) {
                try {
                    objectOutputStream.writeObject(order);
                    objectOutputStream.flush();
                    count++;
                } catch (NotSerializableException e) {
                    System.err.println("The class does not support serialization");
                    e.printStackTrace();
                } catch (IOException e) {
                    System.err.println("Output error");
                    e.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("The file could not be created");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Output error");
            e.printStackTrace();
        }

        return count;
    }
}
