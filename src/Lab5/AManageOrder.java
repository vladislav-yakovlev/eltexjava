package Lab5;

import Lab2and3.Order;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;

public abstract class AManageOrder implements IOrder {
    protected String path;
    protected TreeSet<Order> orders = new TreeSet<>();

    public AManageOrder() {
    }

    public AManageOrder(String path) {
        this.path = path;
    }

    public boolean clearFile() {
        try (PrintWriter printWriter = new PrintWriter(path)) {
            printWriter.print("");
            return true;
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
            e.printStackTrace();
        }

        return false;
    }

    public void removeOrder(Order order) {
        orders.remove(order);
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public TreeSet<Order> getOrders() {
        return orders;
    }

    public List<Order> getOrdersAsArrayList() {
        List<Order> orders = new ArrayList<Order>();
        orders.addAll(this.orders);
        return orders;
    }

    public void setOrders(Collection<Order> orders) {
        this.orders.addAll(orders);
    }

    public void addOrder(Order order) {
        orders.add(order);
    }
}
