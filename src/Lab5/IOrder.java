package Lab5;

import Lab2and3.Order;

import java.util.ArrayList;
import java.util.UUID;

public interface IOrder {

    Order readById(UUID id);

    ArrayList<Order> readAll();

    boolean saveById(UUID id);

    int saveAll();
}
