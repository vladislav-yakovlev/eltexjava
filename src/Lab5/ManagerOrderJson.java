package Lab5;

import Lab2and3.Order;
import Lab2and3.Orders;
import Lab2and3.OrdersFactory;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.*;
import java.util.ArrayList;
import java.util.UUID;

//import com.google.gson.Gson;

public class ManagerOrderJson extends AManageOrder implements IOrder {
    private final String defaultPath = "jsonFile.json";

    public ManagerOrderJson() {
        this.path = defaultPath;
    }

    public ManagerOrderJson(String path) {
        super(path);
    }

    @Override
    public Order readById(UUID id) {
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            ObjectMapper mapper = new ObjectMapper();
            Order order;
            String line;
            while ((line = reader.readLine()) != null) {
                try {
                    order = mapper.readValue(line, new TypeReference<Order>() {
                    });
                    if (order.getId().compareTo(id) == 0) {
                        return order;
                    }
                } catch (JsonParseException e) {
                    e.printStackTrace();
                } catch (JsonMappingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public ArrayList<Order> readAll() {
        ObjectMapper mapper = new ObjectMapper();
        ArrayList<Order> result = new ArrayList<>();
        String line;

        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            while ((line = reader.readLine()) != null)
                try {
                    result.add(mapper.readValue(line, new TypeReference<Order>() {
                    }));
                } catch (JsonParseException e) {
                    e.printStackTrace();
                } catch (JsonMappingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean saveById(UUID id) {
        ObjectMapper mapper = new ObjectMapper();
        for (Order order : orders) {
            if (order.getId().compareTo(id) == 0) {
                //boolean flag = false;
                try (BufferedWriter writer = new BufferedWriter(new FileWriter(path, true))) {
                    String line = mapper.writeValueAsString(order);
                    writer.write(line);
                    writer.flush();
                    writer.newLine();
                    return true;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (JsonGenerationException e) {
                    e.printStackTrace();
                } catch (JsonMappingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //return flag;
            }
        }

        return false;
    }

    @Override
    public int saveAll() {
        int count = 0;
        ObjectMapper mapper = new ObjectMapper();
        //mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path, true))) {
            try {
                // Convert objects to JSON strings and save into the file as lines
                for (Order order : orders) {
                    //StringWriter line;
                    String line = mapper.writeValueAsString(order);
                    writer.write(line);
                    writer.flush();
                    writer.newLine();
                    count++;
                }
            } catch (JsonGenerationException e) {
                e.printStackTrace();
            } catch (JsonMappingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }
}