package Lab1.Devices;

import Lab1.Randomizer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Scanner;

public class TabletComputer extends Device implements ICrudAction, Serializable {
    private String videoProcessor;
    private String screenResolution;

    public TabletComputer() {
        super();
    }

    public TabletComputer(String name, BigDecimal price, String manufacturer, String model, String operatingSystem, String videoProcessor, String screenResolution) {
        super(name, price, manufacturer, model, operatingSystem);


    }

    public String toString() {
        return "[TabletComputer]\n"
                + super.toString()
                + "video processor: " + videoProcessor + "\n"
                + "screen resolution: " + screenResolution;
    }

    public String getVideoProcessor() {
        return videoProcessor;
    }

    public void setVideoProcessor(String videoProcessor) {
        this.videoProcessor = videoProcessor;
    }

    public String getScreenResolution() {
        return screenResolution;
    }

    public void setScreenResolution(String screenResolution) {
        this.screenResolution = screenResolution;
    }

    @Override
    public void create() throws Exception {
        super.create();

        this.screenResolution = Randomizer.getRandomScreenResolution();
        this.videoProcessor = Randomizer.getRandomName();
    }

    @Override
    public void read() {
        System.out.print(this.toString());
    }

    @Override
    public void update() {
        super.update();

        Scanner in = new Scanner(System.in);

        System.out.println("Enter the screen resolution: ");
        setScreenResolution(in.nextLine());

        System.out.println("Enter the name of the video processor: ");
        setVideoProcessor(in.nextLine());
    }

    @Override
    public void delete() {
        super.delete();

        this.screenResolution = null;
        this.videoProcessor = null;
    }
}
