package Lab1.Devices;

public interface ICrudAction {
    public void create() throws Exception;

    public void read();

    public void update();

    public void delete();
}
