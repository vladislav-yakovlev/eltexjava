package Lab1.Devices;

import Lab1.Randomizer;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Scanner;
import java.util.UUID;

@JsonTypeInfo(
        use      = JsonTypeInfo.Id.CLASS,
        include  = JsonTypeInfo.As.PROPERTY,
        property = "type"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Phone.class),
        @JsonSubTypes.Type(value = SmartPhone.class),
        @JsonSubTypes.Type(value = TabletComputer.class),
})

public abstract class Device implements ICrudAction, Serializable {
    protected static int count = 0;

    protected UUID id;
    protected String name;
    protected BigDecimal price;
    protected String manufacturer;
    protected String model;
    protected String operatingSystem;

    public Device() {

    }

    /**
     * @param name            name of the device.
     * @param price           price of the device. This value can not be lower than zero.
     * @param manufacturer    manufacturer of the device.
     * @param model           name of the model of the device.
     * @param operatingSystem name of the operating system on the device.
     */
    public Device(String name, BigDecimal price, String manufacturer, String model, String operatingSystem) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.price = price;
        this.manufacturer = manufacturer;
        this.model = model;
        this.operatingSystem = operatingSystem;

        count++;
    }

    @Override
    public String toString() {
        if (isCreated()) {
            return "id: " + id.toString() + "\n"
                    + "name: " + name + "\n"
                    + "price: " + price.toString() + "\n"
                    + "manufacturer: " + manufacturer + "\n"
                    + "model: " + model + "\n"
                    + "operating system: " + operatingSystem + "\n";
        } else {
            return "The object has not been created!";
        }
    }

    boolean isCreated() {
        if (name != null) {
            return true;
        } else {
            return false;
        }
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Device.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        if (price.compareTo(BigDecimal.valueOf(0)) != -1) {
            this.price = price;
        } else {
            this.price = BigDecimal.valueOf(0);
        }
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    @Override
    public void create() throws Exception {
        //this(Randomizer.getName(), BigDecimal.valueOf(1000), Randomizer.getName(), Randomizer.getName(), Randomizer.getName());

        if (!isCreated()) {
            //this.id = (long) UUID.nameUUIDFromBytes(ByteBuffer.allocate(8).putLong(id).array());
            this.id = UUID.randomUUID();
            this.name = Randomizer.getRandomName();
            this.price = Randomizer.getRandomBigDecimal();
            this.manufacturer = Randomizer.getRandomName();
            this.model = Randomizer.getRandomName();
            this.operatingSystem = Randomizer.getRandomName();

            count++;
        } else {
            throw new Exception("The object already exists!\n");
        }
    }

    @Override
    public void read() {
        System.out.print(this.toString());
    }

    @Override
    public void update() {
        if (!isCreated()) {
            this.id = UUID.randomUUID();
            count++;
        }

        Scanner in = new Scanner(System.in);

        System.out.println("Enter the device name: ");
        setName(in.nextLine());

        System.out.println("Enter the device price: ");
        setPrice(BigDecimal.valueOf(in.nextInt()));

        System.out.println("Enter the device manufacturer: ");
        setManufacturer(in.nextLine());

        System.out.println("Enter the device model name: ");
        setModel(in.nextLine());

        System.out.println("Enter the device operating system name: ");
        setOperatingSystem(in.nextLine());
    }

    @Override
    public void delete() {
        this.id = null;
        this.name = null;
        this.price = null;
        this.manufacturer = null;
        this.model = null;
        this.operatingSystem = null;

        count--;
    }
}
