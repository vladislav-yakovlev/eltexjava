package Lab1.Devices;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Scanner;

public class Phone extends Device implements ICrudAction, Serializable {
    private static String[] caseTypes = {"classic", "clamshell"};

    protected String caseType;

    public Phone() {
        super();
    }

    public Phone(String name, BigDecimal price, String manufacturer, String model,
                 String operatingSystem, String caseType) {
        super(name, price, manufacturer, model, operatingSystem);
        setCaseType(caseType);
    }

    public String toString() {
        return "[Phone]\n"
                + super.toString()
                + "case type: " + caseType;
    }

    private int checkTypeOfCase(String caseType) {

        for (int i = 0; i < caseTypes.length; i++) {
            if (caseType.toLowerCase().compareTo(caseTypes[i].toLowerCase()) == 0) {
                return i;
            }
        }

        return 0;
    }

    public static String[] getCaseTypes() {
        return caseTypes;
    }

    public static void setCaseTypes(String[] caseTypes) {
        Phone.caseTypes = caseTypes;
    }

    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseTypes[checkTypeOfCase(caseType)];
    }

    public void create() throws Exception {
        super.create();

        this.caseType = caseTypes[(int) (Math.random() * caseTypes.length)];
    }

    public void read() {
        System.out.print(this.toString());
    }

    public void update() {
        super.update();

        Scanner in = new Scanner(System.in);

        System.out.println("Enter the phone case type: ");
        setCaseType(in.nextLine());
    }

    public void delete() {
        super.delete();

        this.caseType = null;
    }
}
