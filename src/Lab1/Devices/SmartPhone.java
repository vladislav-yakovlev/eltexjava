package Lab1.Devices;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Scanner;

public class SmartPhone extends Device implements ICrudAction, Serializable {
    private String[] typesOfSIM = {"common", "micro-SIM"};

    protected String typeOfSIM;
    protected int numberOfSIM;

    public SmartPhone() {
        super();
    }

    public SmartPhone(String name, BigDecimal price, String manufacturer, String model, String operatingSystem, String typeOfSIM, int numberOfSIM) {
        super(name, price, manufacturer, model, operatingSystem);

        setTypeOfSIM(typeOfSIM);
        setNumberOfSIM(numberOfSIM);
    }

    public String toString() {
        return "[SmartPhone]\n"
                + super.toString()
                + "type of SIM: " + typeOfSIM + "\n"
                + "number of SIM: " + numberOfSIM;
    }

    private int checkTypeOfSIM(String typeOfSIM) {
        for (int i = 0; i < typesOfSIM.length; i++) {
            if (typeOfSIM.toLowerCase().compareTo(typesOfSIM[i].toLowerCase()) == 0) {
                return i;
            }
        }

        return 0;
    }

    public String[] getTypesOfSIM() {
        return typesOfSIM;
    }

    public void setTypesOfSIM(String[] typesOfSIM) {
        this.typesOfSIM = typesOfSIM;
    }

    public String getTypeOfSIM() {
        return typeOfSIM;
    }

    public void setTypeOfSIM(String typeOfSIM) {
        this.typeOfSIM = typesOfSIM[checkTypeOfSIM(typeOfSIM)];
    }

    public int getNumberOfSIM() {
        return numberOfSIM;
    }

    public void setNumberOfSIM(int numberOfSIM) {
        if (numberOfSIM > 0) {
            this.numberOfSIM = numberOfSIM;
        } else {
            this.numberOfSIM = 1;
        }
    }

    public void create() throws Exception {
        super.create();

        this.typeOfSIM = typesOfSIM[(int) (Math.random() * typesOfSIM.length)];
        this.numberOfSIM = (int) Math.random() * 2 + 1;
    }

    public void read() {
        System.out.print(this.toString());
    }

    public void update() {
        super.update();

        Scanner in = new Scanner(System.in);

        System.out.println("Enter the type of SIM: ");
        setTypeOfSIM(in.nextLine());

        System.out.println("Enter the number of SIM cards: ");
        setNumberOfSIM(in.nextInt());
    }

    public void delete() {
        super.delete();

        this.typeOfSIM = null;
        this.numberOfSIM = 0;
    }
}
