package Lab1;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

public class Randomizer {
    private static String[] lib =
            {"na", "ma", "mo", "be", "qe", "i", "mu", "bro", "o", "s"};
    private static String[] domains =
            {".com", ".fr", ".uk", ".ve", ".jp", ".se", ".pt", ".gr", ".ru"};
    private static int maxPrice = 10000;

    private static void addName(StringBuilder string) {
        for (int i = 0; i <= Math.random() * 10 + 1; i++) {
            string.append(lib[(int) (Math.random() * lib.length)]);
        }
    }

    public static String getRandomName() {
        StringBuilder res = new StringBuilder();

        addName(res);
        res.setCharAt(0, Character.toUpperCase(res.charAt(0)));

        return res.toString();
    }

    public static String getRandomEmail() {
        StringBuilder res = new StringBuilder();

        addName(res);
        res.append("@");
        addName(res);
        res.append(domains[new Random().nextInt(domains.length)]);

        return res.toString();
    }

    public static String getRandomScreenResolution() {
        return (256 * (int) (Math.random() * 10 + 1)) + "x" + (256 * (int) (Math.random() * 10 + 1));
    }

    public static BigDecimal getRandomBigDecimal() {
        if (maxPrice > 0) {
            return BigDecimal.valueOf(Math.random() * (maxPrice - 1) + Math.random()).setScale(2, RoundingMode.HALF_DOWN);
            //setScale(2, BigDecimal.ROUND_HALF_DOWN);
        } else {
            return BigDecimal.valueOf(0);
        }
    }
}
