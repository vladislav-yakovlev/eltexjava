package Lab1;

import Lab2and3.Order;
import Lab2and3.Orders;
import Lab2and3.OrdersFactory;
import Lab5.ManagerOrderFile;
import Lab5.ManagerOrderJson;
import Lab6.TestClient;
import Lab6.TestServer;
import Lab6.Client;
import Lab6.Server;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Helper {
    public static void testManagerOrderFile() {
        ManagerOrderFile managerOrderFile = new ManagerOrderFile("byteFile.dat");
        managerOrderFile.clearFile();
        managerOrderFile.addOrder(OrdersFactory.getOrderWithId(UUID.fromString("0000-0000-0000-0000-0000")));
        managerOrderFile.addOrder(OrdersFactory.getOrderWithId(UUID.fromString("1111-1111-1111-1111-1111")));
        managerOrderFile.addOrder(OrdersFactory.getOrderWithId(UUID.fromString("2222-2222-2222-2222-2222")));
        managerOrderFile.addOrder(OrdersFactory.getOrderWithId(UUID.fromString("3333-3333-3333-3333-3333")));
        managerOrderFile.addOrder(OrdersFactory.getOrderWithId(UUID.fromString("4444-4444-4444-4444-4444")));
        int all = managerOrderFile.saveAll();
        boolean savedById = managerOrderFile.saveById(UUID.fromString("1111-1111-1111-1111-1111"));

        ArrayList<Order> orders = managerOrderFile.readAll();
        Order order = managerOrderFile.readById(UUID.fromString("3333-3333-3333-3333-3333"));
        System.out.println(all + " " + savedById + " " + orders.size());
        System.out.println(order);
    }

    public static void testManagerOrderJson() {
        ManagerOrderJson managerOrderJson = new ManagerOrderJson("JSONFile.json");
        managerOrderJson.clearFile();
        managerOrderJson.addOrder(OrdersFactory.getOrderWithId(UUID.fromString("0000-0000-0000-0000-0000")));
        managerOrderJson.addOrder(OrdersFactory.getOrderWithId(UUID.fromString("1111-1111-1111-1111-1111")));
        managerOrderJson.addOrder(OrdersFactory.getOrderWithId(UUID.fromString("2222-2222-2222-2222-2222")));
        managerOrderJson.addOrder(OrdersFactory.getOrderWithId(UUID.fromString("3333-3333-3333-3333-3333")));
        managerOrderJson.addOrder(OrdersFactory.getOrderWithId(UUID.fromString("4444-4444-4444-4444-4444")));
        int all = managerOrderJson.saveAll();
        boolean savedById = managerOrderJson.saveById(UUID.fromString("1111-1111-1111-1111-1111"));

        List<Order> orders = managerOrderJson.readAll();
        Order order = managerOrderJson.readById(UUID.fromString("3333-3333-3333-3333-3333"));
        System.out.println(all + " " + savedById + " " + orders.size());
        System.out.println(order);
    }

    public static void testJson() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        StringWriter line = new StringWriter();
        try {
            Order orderToString = OrdersFactory.getRandomOrder();
            //Convert object to JSON string and save
            mapper.writeValue(line, orderToString);
            System.out.println(line);

            //Convert JSON string to object and write to console
            Order orderFromString = mapper.readValue(line.toString(), new TypeReference<Order>() {
            });
            System.out.println(orderFromString);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void testNetwork() {
        Orders orders = new Orders();
        int clientsNumber = 5;
        ExecutorService executorService = Executors.newFixedThreadPool(6);
        executorService.submit(new Server(clientsNumber, 50000, orders));
        for (int i = 1; i <= 5; i++) {
            executorService.submit(new Client(50000 + clientsNumber + i, 50000 + i));
        }
        executorService.shutdown();
    }

    public static void testUdp2() {
        int port = 50001;
        TestServer testServer = new TestServer(port);
        TestClient testClient = new TestClient(port);

        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(testClient);
        executorService.submit(testServer);
    }
}
