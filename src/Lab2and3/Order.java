package Lab2and3;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class Order implements Serializable, Comparable<Order> {
    protected UUID id;
    protected boolean status; //false - pending, true - processed
    protected Date creationTime;
    protected Date pendingTime;

    protected Credentials client;
    protected ShoppingCart cart;

    public Order() {
        creationTime = new Date(); //Current time
        pendingTime = new Date(creationTime.getTime() + 1000);
    }

    public Order(UUID id) {
        this();
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("id: " + id.toString() + "\n");
        string.append("status: " + status + "\n");
        string.append("creation time: " + creationTime.toString() + "\n");
        string.append("pending time: " + pendingTime.toString() + "\n");
        string.append(client.toString() + "\n");
        string.append("list of products:\n");
        string.append(cart.toString());

        return string.toString();
    }

    @Override
    public int compareTo(Order order) {
        /*if (order != null)
            return this.id.compareTo(order.id);
        else*/
        return this.id.compareTo(order.getId());
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Date getPendingTime() {
        return pendingTime;
    }

    public void setPendingTime(Date pendingTime) {
        this.pendingTime = pendingTime;
    }

    public Credentials getClient() {
        return client;
    }

    public void setClient(Credentials client) {
        this.client = client;
    }

    public ShoppingCart getCart() {
        return cart;
    }

    public void setCart(ShoppingCart cart) {
        this.cart = cart;
    }
}
