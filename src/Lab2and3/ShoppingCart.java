package Lab2and3;

import Lab1.Devices.Device;

import java.io.Serializable;
import java.util.*;

public class ShoppingCart<T extends Device> implements Serializable {
    //protected Collection<Device> products = new LinkedList<>();
    protected List<T> products = new LinkedList<>();
    protected HashSet<UUID> idSet = new HashSet<>();


    public ShoppingCart() {
    }

    @Override
    public String toString() {
        if (products.size() != 0) {
            StringBuilder string = new StringBuilder();
            for (T product : products) {
                string.append(product.toString() + "\n");
            }
            return string.toString();
        }

        return "Shopping cart is empty!";
    }

    void add(T product) {
        products.add(product);
        idSet.add(product.getId());
    }

    void delete(T product) {
        products.remove(product);
        idSet.remove(product.getId());
    }

    T find(UUID id) {
        if (idSet.contains(id)) {
            for (int i = 0; i < products.size(); i++) {
                if (products.get(i).getId().compareTo(id) == 0) {
                    return products.get(i);
                }
            }
        }

        return null;
    }

    void showAllProducts() {
        if (products.size() != 0) {
            for (T product : products) {
                System.out.println(product);
            }
        } else {
            System.out.println("Shopping cart is empty!");
        }
    }

    public void removeAllProducts() {
        products.clear();
        idSet.clear();
    }

    public void addAllProducts(Collection<T> products) {
        this.products.addAll(products);

        for (T product : products) {
            idSet.add(product.getId());
        }
    }

    public List<T> getProducts() {
        return products;
    }

    public void setProducts(List<T> products) {
        this.products = products;
    }

    public HashSet<UUID> getIdSet() {
        return idSet;
    }

    public void setIdSet(HashSet<UUID> idSet) {
        this.idSet = idSet;
    }
}
