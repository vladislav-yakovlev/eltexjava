package Lab2and3;

import Lab1.Devices.ICrudAction;
import Lab1.Randomizer;

import java.io.Serializable;
import java.util.Scanner;
import java.util.UUID;

public class Credentials implements ICrudAction, Serializable {
    protected UUID id;
    protected String surname;
    protected String name;
    protected String patronymic;
    protected String email;

    public Credentials() {

    }

    @Override
    public String toString() {
        if (isCreated()) {
            return "id: " + id.toString() + "\n"
                    + "surname: " + surname + "\n"
                    + "name: " + name + "\n"
                    + "patronymic: " + patronymic + "\n"
                    + "email: " + email + "\n";
        } else {
            return "The object has not been created!";
        }
    }

    boolean isCreated() {
        if (name != null) {
            return true;
        } else {
            return false;
        }
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public void create() throws Exception {
        if (!isCreated()) {
            this.id = UUID.randomUUID();
            this.surname = Randomizer.getRandomName();
            this.name = Randomizer.getRandomName();
            this.patronymic = Randomizer.getRandomName();
            this.email = Randomizer.getRandomEmail();
        } else {
            throw new Exception("The object already exists!\n");
        }
    }

    @Override
    public void read() {
        System.out.print(this.toString());
    }

    @Override
    public void update() {
        if (!isCreated()) {
            this.id = UUID.randomUUID();
        }

        Scanner in = new Scanner(System.in);

        System.out.println("Enter the user's surname: ");
        setSurname(in.nextLine());

        System.out.println("Enter the user's name: ");
        setName(in.nextLine());

        System.out.println("Enter the user's patronymic: ");
        setPatronymic(in.nextLine());

        System.out.println("Enter user email: ");
        setEmail(in.nextLine());
    }

    @Override
    public void delete() {
        this.id = null;
        this.surname = null;
        this.name = null;
        this.patronymic = null;
        this.email = null;
    }
}
