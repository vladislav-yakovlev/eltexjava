package Lab2and3;

import Lab1.Devices.Device;
import Lab1.Devices.Phone;
import Lab1.Devices.SmartPhone;
import Lab1.Devices.TabletComputer;

import java.util.Random;
import java.util.UUID;

public class OrdersFactory {
    public static Order getRandomOrder() {
        Order order = new Order(UUID.randomUUID());
        try {
            order.setStatus(new Random().nextBoolean());
            order.setClient(new Credentials());
            order.getClient().create();

            order.setCart(new ShoppingCart());
            for (int i = 0; i < new Random().nextInt(4) + 1; i++) {
                Device device;
                switch (new Random().nextInt(3)) {
                    case 0: {
                        device = new Phone();
                    }
                    break;
                    case 1: {
                        device = new SmartPhone();
                    }
                    break;
                    default: {
                        device = new TabletComputer();
                    }
                }

                device.create();
                order.getCart().add(device);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return order;
    }

    public static Order getOrderWithId(UUID id)
    {
        Order order = new Order(id);
        try {
            order.setStatus(new Random().nextBoolean());
            order.setClient(new Credentials());
            order.getClient().create();

            order.setCart(new ShoppingCart());
            for (int i = 0; i < new Random().nextInt(4) + 1; i++) {
                Device device;
                switch (new Random().nextInt(3)) {
                    case 0: {
                        device = new Phone();
                    }
                    break;
                    case 1: {
                        device = new SmartPhone();
                    }
                    break;
                    default: {
                        device = new TabletComputer();
                    }
                }

                device.create();
                order.getCart().add(device);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return order;
    }
}
