package Lab2and3;

import Lab5.IOrder;

import java.util.*;

public class Orders<O extends Order> {
    protected List<O> orders;
    protected TreeMap<Date, O> removedOrders;

    public Orders() {
        this.orders = new ArrayList<O>();
    }

    public void makePurchase(O order) {
        orders.add(order);
    }

    public Order getOrder(UUID id) {
        for (Order order : orders) {
            if (order.getId().compareTo(id) == 0) {
                return order;
            }
        }

        return null;
    }

    public void checkOrders() {
        for (int i = 0; i < orders.size(); i++) {
            if (orders.get(i).getStatus() == false) {
                orders.get(i).setStatus(true);
            }
        }
    }

    public void removeProcessedOrders() {
        for (int i = 0; i < orders.size(); i++) {
            if (orders.get(i).getStatus() == true && orders.get(i).getPendingTime().getTime() <= new Date().getTime()) {
                O order = orders.remove(i);
                removedOrders.put(order.getPendingTime(), order);
            }
        }
    }

    public List<O> getOrders() {
        return orders;
    }

    public void setOrders(List<O> orders) {
        this.orders = orders;
    }

    public TreeMap<Date, O> getRemovedOrders() {
        return removedOrders;
    }

    public void setRemovedOrders(TreeMap<Date, O> removedOrders) {
        this.removedOrders = removedOrders;
    }
}
